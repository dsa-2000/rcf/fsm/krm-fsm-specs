# Overview (RFSoC Concept)

The FPGA Station Module (FSM) is a carrier board designed to interface a pair of analog-over-fiber RF signals with an FPGA performing digital signal processing. The FSM emits digital data over high speed (25 or 100Gb/s) Ethernet.

The FSM hosts an FPGA System on Module (SoM) integrating a high speed dual-channel ADC, and a pair of Fiber Receiver (FRX) boards.

A top-level block diagram of the FSM is shown below:

![Block diagram](./images/rcf-fsm-rfsoc.drawio.png)


# Physical Specifications

## Board configuration

1. 100 mm Eurocard height
2. 160 mm Eurocard length (longer standard depths, eg 220 mm also possible)
3. Designed for mounting vertically in 3U subrack. Board edges should have adequate keepout zones to accomodate card guide rails.
4. Custom heatsink required for bottom to top airflow
5. Designed to operate with a half-height backplane. QSFP/RJ45 connectors and LEDs should be mounted on the upper side of the board.
6. Mounting holes to accomodate operation in non-card-cage environments.

### FRX interface

The FRX board top-down view is shown below:

![FRX top-down view](./images/frx-top.png)

The FRX board is installed top-down on the FRX. A view of an FRX, side-on, as installed on the FSM is shown below:

![FRX side-on view](./images/frx-side-facedown.png)

A dimensioned drawing of the FRX board is available [here](./images/frx-top-mechanical.pdf).


1. The FSM-side of the FRX-FSM interface is a smooth-bore surface mount SMP eg [Amphenol SMP-MSSB-PCS15T](https://www.amphenolrf.com/smp-mssb-pcs15t.html)

2. Power and control signals are delivered to the FRX via a 2x4 0.1" header. The pinout of the male header on the FRX board is:
	- pin 1: I2C SCL (3.3V)
	- pin 2: I2C SDA (3.3V)
	- pin 3: GND
	- pin 4: VDD (6.5 - 18V) 
	- pin 5: GND
	- pin 6: VDD (6.5 - 18V)
	- pin 7: Not Connected
	- pin 8: Not Connected

3. In operation, an FRX module is expected to draw 200 mA. The FRX has linear power regulators, so the power dissipation is expected to be between 1.3 and 3.6 W, depending on the input voltage (6.5 - 18V).

4. The FSM board should pull up SDA and SCL lines to 3.3V, per the I2C spec.

5. Each of the two FRX boards should have an individual I2C interface to the PS side of the SoM FPGA.

### Backplane interface

The FSM board receives power and control signals from a custom backplane.

1. The backplane-side connector is Molex [761651328](https://www.molex.com/en-us/products/part-detail/0761651328). The mating part for the FSM board is Molex [761701036](https://www.molex.com/en-us/products/part-detail/0761701036). Pinout of the backplane-side connector is shown below.

![FSM backplane connector pinout](./images/fsm-backplane-pinout.png)

### PLL Interface

The FSM uses an RFSoC digitizer, with external clock generation designed to be used to synchronize multiple boards.
Various possible clocking implementations are possible, for example TI LMK04828 + LMX2594 or ADI HMC7044.
However, any chosen implementation must be phase-deterministic with respect to an external synchronization pulse provided by the FSM backplane.

Likely PLL FSM connections are:

1. SDIO -- SoM PS SPI MOSI
2. SDO -- SoM PS SPI MISO
3. SCLK -- SoM PS SPI SCLK
4. CSB -- SoM PS SPI CSB (possibly multiple chip selects if more than one PLL chip is required)
5. PLL Master reference -- REFP from backplane
6. PLL Master reference (inverted) -- REFN from backplane
7. PLL sync pulse -- SYNCP from backplane
8. PLL sync pulse (inverted) -- SYNCN from backplane

### SoM Interface

Some SoM signals shoudl be directly connected to the backplane as follows:

1. SoM Power Enable - PENABLE from backplane. A jumper install option should be provided to allow power enable to be permanently pulled high.
2. SoM FPGA UART0 - micro USB connector
3. SoM FPGA UART1 - UART1 from backplane
4. SoM SD card - micro SD card slot
5. SoM PS GPIO - SLOT0/SLOT1/SLOT2/SLOT3 from backplane
6. SoM PL GPIO, 3V3 capable - "spare" pins on backplane, via 0-ohm resistors
7. SoM PL GPIO, 3V3 capable - 5 x 2 (or similar) 0.1" header

# Functional Requirements

## Backplane interface

1. The FSM board should communicate via 1000-BASEX over the backplane. This interface should be available to the SoM FPGA PS, and may require an external PHY and/or reference clock.

2. The FSM board should communicate via 10/100/1000 BASE-T via an RF45 connector. This interface should be available to the SoM FPGA PS.

3. The FSM is powered from a single supply at 12V, and up to 9A. *Ideally the FSM would tolerate higher voltages, eg. 24V*

4. The backplane I2C interface is connected to a several devices:
	1. I2C GPIO expander, driving:
		1. SoM warm reset PS_SRST_B (Pulled up so as to default to not reset)
		2. SoM CFG_OKn
		3. SoM PS_RESET_INn
		4. SoM HW_RESET_INn
		5. SoM RESET_OUT
		6. SoM EXT_PWR_OK
		2. 10-bits GPIO to the SoM PS (or as many bits as possible to the PS and remainder to PL)
	2. I2C EEPROM with 48-bit MAC address (eg. [Microchip 24AA02E48](https://www.microchip.com/en-us/product/24AA02E48))


## SoM interface

1. The SoM PS should have access to a small I2C EEPROM with 48-bit MAC. Eg [Microchip 24AA02E48](https://www.microchip.com/en-us/product/24AA02E48)
2. The SoM PS should have access to two temperature sensors distributed on the FSM board
3. The SoM should be capable of monitoring current draw from the backplane connector
4. 2 SoM GTY banks should be broken out to QSFP+ cages. These should be capable of 25Gb/s operation per lane (i.e. 100GbE / 25GbE standards).
5. QSFP cage's I2C interfaces should be connected to PL GPIO pins on the SoM.
7. A programmable clock generator should be available to the SoM capable of generating GTH/GTY reference clocks for the ADC JESD interface, 100GbE interfaces, and any necessary 1GbE references.
8. 1 bicolor (red/green) LED on front edge of the board should be connected to the SoM *PS* GPIO.
9. 1 bicolor (red/green) LED on the read edge of the board should be connected to the SoM *PS* GPIO.
10. 1 bicolor (red/green) LED on front edge of the board should be connected to the SoM *PL* GPIO.
11. 1 bicolor (red/green) LED on the rear edge of the board should be connected to the SoM *PL* GPIO.
